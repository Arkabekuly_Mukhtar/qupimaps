package mukhtar.qupimap.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Mukhtar on 3/21/2017.
 */

public class Route {

    public double latitudeStart;
    public double longitudeStart;

    public  double latitudeEnd;
    public  double longitudeEnd;

    public String distanceInKm;
    public int durationInMin;
    public String startAddress;
    public String endAddress;

    public List<LatLng> polyline;

    public Route(JSONObject route) throws JSONException{

        polyline = PolyUtil.decode(route.getJSONObject("overview_polyline").getString("points"));

        JSONArray legs = route.getJSONArray("legs");
        JSONObject current_leg = legs.getJSONObject(0);

        JSONObject distance = current_leg.getJSONObject("distance");
        distanceInKm = distance.getString("text");

        JSONObject duration = current_leg.getJSONObject("duration");
        durationInMin = duration.getInt("value");

        endAddress = current_leg.getString("end_address");
        startAddress = current_leg.getString("start_address");

        latitudeStart = current_leg.getJSONObject("start_location").getDouble("lat");
        longitudeStart = current_leg.getJSONObject("start_location").getDouble("lng");
        latitudeEnd = current_leg.getJSONObject("end_location").getDouble("lat");
        longitudeEnd = current_leg.getJSONObject("end_location").getDouble("lng");
    }
}
