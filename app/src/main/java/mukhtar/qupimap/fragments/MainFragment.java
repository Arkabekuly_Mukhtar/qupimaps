package mukhtar.qupimap.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import mukhtar.qupimap.R;
import mukhtar.qupimap.model.Route;

import static mukhtar.qupimap.Additional.Constants.LOG_TAG;


public class MainFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    LinearLayout place;
    TextView show_distance;
    ProgressDialog pd;

    private final String STATUS_OK = "OK";
    private final String STATUS_NOT_FOUND = "NOT_FOUND";
    private final String STATUS_ZERO_RESULTS = "ZERO_RESULTS";
    private final String STATUS_UNKNOWN_ERROR = "UNKNOWN_ERROR";
    private final String MODE_DRIVING = "driving";
    private final String MODE_WALKING = "walking";


    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance() {
        MainFragment fragment =new MainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        place = (LinearLayout) view.findViewById(R.id.linear_layout_place);
        show_distance = (TextView) view.findViewById(R.id.button_distance);

        ImageView tricolon = (ImageView) view.findViewById(R.id.image_view_tricolon);




        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d("myLogs","inside onmap ready ");
    }

    public void getDistance(LatLng latLng){

        pd = new ProgressDialog(getContext());
        pd.setTitle("Карта");
        pd.setMessage("Загрузка...");
        // добавляем кнопку
        pd.setCancelable(false);
        pd.show();

        AsyncForRoute routeToMap = new AsyncForRoute(latLng.latitude, latLng.longitude, 43.243817, 76.849885, MODE_DRIVING);
        routeToMap.execute();
    }


    class AsyncForRoute extends AsyncTask<Void,Void,String>{

        private String MODE;
        private double latitude1;
        private double longitude1;
        private double latitude2;
        private double longitude2;
        private String response = "";
        private URL url;
        HttpURLConnection conn;

        public AsyncForRoute(double latitude1, double longitude1, double latitude2, double longitude2, String MODE ) {

            this.latitude1 = latitude1;
            this.longitude1 = longitude1;
            this.latitude2 = latitude2;
            this.longitude2 = longitude2;
            this.MODE = MODE;
        }


        @Override
        protected String doInBackground(Void... voids) {
            try {
                url = new URL("http://maps.googleapis.com/maps/api/directions/json?origin=" +
                        latitude1 + "," + longitude1 + "&destination=" +
                        latitude2 + "," + longitude2 + "&units=metric&mode=" +
                        MODE+"&alternatives=false&language=ru");
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");

                StringBuilder sb = new StringBuilder();
                int HttpResult = conn.getResponseCode();

                if (HttpResult == HttpURLConnection.HTTP_OK) {

                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    Log.d(LOG_TAG, "Json object onResponse: " + sb.toString());
                    response = sb.toString();

                } else {
                    Log.d(LOG_TAG, "Json object Error " + conn.getResponseMessage());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            try {
                JSONObject jsonObject = new JSONObject(response);
                switch (jsonObject.getString("status")){
                    case STATUS_OK:
                        JSONArray array = jsonObject.getJSONArray("routes");
                        JSONObject route = array.getJSONObject(0);

                        Route myRoute = new Route(route);
                        setStartDestinationMarkers(myRoute);
                        addPolylineToMap(myRoute);
                        changeViewContent(myRoute);
                        pd.cancel();

                        break;
                    case STATUS_NOT_FOUND:
                        pd.setCancelable(true);
                        pd.setMessage("Места не нашлось");
                        break;
                    case STATUS_ZERO_RESULTS:
                        pd.setCancelable(true);
                        pd.setMessage("Нет никаких маршрутов");
                        break;
                    case STATUS_UNKNOWN_ERROR:
                        pd.setCancelable(false);
                        pd.setMessage("Сервер не отвечает");
                        break;
                    case "":
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setStartDestinationMarkers(Route route){

        int dps = 20;
        final float scale = getContext().getResources().getDisplayMetrics().density;
        int pixels = (int) (dps * scale + 0.5f);

        MarkerOptions markerStart = new MarkerOptions().position(new LatLng(route.latitudeStart,route.longitudeStart));
        BitmapDrawable icon = (BitmapDrawable)ContextCompat.getDrawable(getContext(),R.drawable.location_pointer_user);
        Bitmap imageBitmapS = Bitmap.createScaledBitmap(icon.getBitmap(),pixels,pixels,true);
        markerStart.icon(BitmapDescriptorFactory.fromBitmap(imageBitmapS));
        mMap.addMarker(markerStart);

        MarkerOptions markerDestination = new MarkerOptions().position(new LatLng(route.latitudeEnd,route.longitudeEnd));
        BitmapDrawable iconDestination = (BitmapDrawable)ContextCompat.getDrawable(getContext(),
                R.drawable.location_pointer_destination);
        Bitmap imageBitmapD = Bitmap.createScaledBitmap(iconDestination.getBitmap(),pixels,pixels,true);
        markerDestination.icon(BitmapDescriptorFactory.fromBitmap(imageBitmapD));
        mMap.addMarker(markerDestination);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.latitudeStart,route.longitudeStart), 12));



    }

    private void addPolylineToMap(Route route){
        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE);
        options.addAll(route.polyline);
        mMap.addPolyline(options);
    }

    private void changeViewContent(Route route){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),"sfns_display.ttf");

        TextView address_start = (TextView) place.findViewById(R.id.textview_address_start);
        address_start.setText(route.startAddress);
        address_start.setTypeface(typeface);

        TextView address_destination = (TextView) place.findViewById(R.id.textview_address_destination);
        address_destination.setText(route.endAddress);
        address_destination.setTypeface(typeface);

        TextView time_by_car = (TextView) place.findViewById(R.id.time_by_car);
        int minutes = route.durationInMin/60;
        time_by_car.setText(getStringRepresentationOfMinuteTime(minutes));
        time_by_car.setTypeface(typeface);

        TextView time_by_walk = (TextView) place.findViewById(R.id.time_by_walk);
        time_by_walk.setText(getStringRepresentationOfMinuteTime(minutes*10));
        time_by_walk.setTypeface(typeface);

        String distance = route.distanceInKm.split(" ")[0]+"\n"+route.distanceInKm.split(" ")[1];
        show_distance.setText(distance);
        show_distance.setTypeface(typeface);
    }

    private String getStringRepresentationOfMinuteTime(int minutes){
        String result = "";
        if((minutes+5)/60==0){
            result = (minutes+5)+"-"+minutes+" мин";
        }else{
            result = ((minutes+5)/60)+" ч. "+((minutes+5)%60)+" мин";
        }
        return  result;
    }

}
